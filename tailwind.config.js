/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter'],
      },
      fontWeight: {
        thin: 100,
        light: 300,
        regular: 400,
        semibold: 600,
        medium: 500,
        bold: 700,
      },
      colors: {
        violet:'#451AA3',
        softviolet: '#7F56D9',
        textgrey: '#212121',
        textgrey2:'#667085',
        textgrey3:'#757575',
        lightgrey: '#D0D5DD',
        textlightgrey: '#98A2B3',
        lightviolet: '#E8DEFC',
        backgroundColor:'#F6F4F7',
        red:'#DE0202',
        green:'#257E49',
        gray3:"#828282"
      },
      backgroundImage: {
        'orange-shade':
          'linear-gradient(81.53deg, #F37920 17.22%, #FF9E5E 86.25%)',
      },
      borderWidth: {
        1: '1px',
      },
      spacing: {
        extrasmall: '7.5rem',
        small: '10rem',
        medium: '12.5rem',
        semimedium: '14rem',
        large: '20rem',
      },
      gridTemplateRows: {
        layout: 'repeat(36, minmax(0, 1fr))',
      },
      animation: {
        'spin-slow': 'spin 1.5s linear infinite',
      },
    },
  },
  plugins: [],
}
