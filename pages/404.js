'use client'
import { useRouter } from "next/navigation";
import Button from "@/components/Button";

export default function _404() {
    const router = useRouter();
    return (
        <div className="flex col-span-full row-span-full justify-center items-center">
            <div className="ml-[3rem]">
                <h1 className="text-[7rem] mb-[1rem] font-extrabold text-lightgrey">404 :/</h1>
                <h2 className="text-[2rem] mb-3">Oops! This Page Could Not Be Found</h2>
                <p className="mb-3">
                SORRY BUT THE PAGE YOU ARE LOOKING FOR DOES NOT EXIST, HAVE BEEN REMOVED, NAME CHANGED OR IS TEMPORARILY UNAVAILABLE.
                </p>
                <span>
                    <Button
                        className="w-[15rem]"
                        onClick={() => {
                            router.push("/");
                        }}
                    >GO TO HOMEPAGE</Button>
                </span>
            </div>
        </div>
    );
}
