import '@/styles/globals.css'
import { Inter } from 'next/font/google';
import Layout from '@/components/Layout';

const inter = Inter({ subsets: ['latin'] })

export const metadata = {
  title: 'Day Zero School',
  description: '',
}
export default function App({ Component, pageProps }) {
  return (
    <main className={inter.className}>
        <Layout><Component {...pageProps} /></Layout>
    </main>
  )
}
