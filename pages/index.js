'use client'

import Image from 'next/image'
import Button from '@/components/Button'
import Input from '@/components/InputFields'
import { useState } from 'react'


export default function Home() {
  const [ctcRange,setCtcRange]= useState([2,80]);
  const [selectedTab,setSelectedTab] = useState(1);
  const [selectedJobDescTab,setSelectedJobTab] = useState(11);

  const tabItems = [
    {
      key: '1',
      label: `All Jobs`,
      children: `Content of Tab Pane 1`,
    },
    {
      key: '2',
      label: `Saved Jobs`,
      children: `Content of Tab Pane 2`,
    },
    {
      key: '3',
      label: `Applied Jobs`,
      children: `Content of Tab Pane 3`,
    },
  ];

  const jobDescriptionTabItems=[
    {
      key: '11',
      label: `Job Description`,
      children: `Content of Tab Pane 1`,
    },
    {
      key: '12',
      label: `Eligibility Criteria`,
      children: `Content of Tab Pane 2`,
    }
  ]

  const onChange = (key) => {
    console.log(key);
  };

  return (
    <div className='w-full h-full'>
      <div className='h-[5.5rem] flex items-center border-b-1 gap-6 px-2'>
        <Input type='text' className="w-[18%] h-[4rem]" label='Job Profile' placeholder='Enter Job Profile' />
        <Input type='text' className="w-[18%] h-[4rem]" label='Industry' placeholder='Your preferred Industry' />
        <Input type='text' className="w-[18%] h-[4rem]" label='Location' placeholder='Your preferred work location' />
        <Input type='switch' className="w-[18%] h-[4rem]" label='Remote Jobs' />
        <Input type='range' className="w-[18%] h-[4rem]" label='CTC' value={ctcRange} />

      </div>
      <div className='flex w-full h-full'>
        <div className='border-r-1 w-[40%] '>
        <span className='flex justify-between text-[16px] items-center h-[2.5rem] border-b-1'>{tabItems.map(item=>(
          <span key={item.key} title={item.label} className={`flex cursor-pointer justify-center w-[33%] overflow-hidden text-ellipsis whitespace-nowrap h-full pt-2 ${selectedTab == item.key?'text-softviolet border-b-softviolet border-b-2 font-medium':'text-textgrey3 font-normal'}`} 
          onClick={()=>setSelectedTab(item.key)}>
            {item.label}</span>
        ))}</span>
        </div>
        <div className='w-[60%]'>
          <div className='h-[8rem] flex flex-col justify-end w-full'>
          <span className='flex text-[16px] items-center h-[2.5rem] border-b-1 mb-[-2px]'>{jobDescriptionTabItems.map(item=>(
          <span key={item.key} className={`flex cursor-pointer justify-center w-[25%]  h-full overflow-hidden text-ellipsis whitespace-nowrap pt-2 ${selectedJobDescTab == item.key?'text-softviolet border-b-softviolet border-b-2 font-medium':'text-textgrey3 font-normal'}`} 
          onClick={()=>setSelectedJobTab(item.key)}>
            {item.label}</span>
        ))}</span>
          </div>

          <div className='h-[5rem] mb-0 border-t-1'></div>
        </div>
      </div>
               </div>

  )
}
