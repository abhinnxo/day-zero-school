'use client'

import Image from 'next/image'
import Button from '@/components/Button'
import { useRouter } from 'next/navigation'

export default function Home() {
    const router=useRouter();
  return (
    <div className='flex w-full h-full justify-center items-center'>
        <span className='flex flex-col mt-[-2rem] ml-[-2rem]'><Image alt="profile-not-completed" 
        src="/incomplete_task.png" width="250" height="250"  className='mx-auto'/>
        <span className='text-softviolet text-[36px] font-semibold mt-[2rem]'>Your profile is not complete</span>
        <span className='text-textgrey3 text-[18px] flex justify-center'>Complete your profile to start applying to jobs</span>
        <Button className='w-[7rem] mx-auto mt-[2rem]' onClick={()=>router.push('/user/my-profile')}>
            My Profile</Button></span>
        
               </div>

  )
}
