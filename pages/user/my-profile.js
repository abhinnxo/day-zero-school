'use client'

import Image from 'next/image'
import Button from '@/components/Button'
import Input from '@/components/InputFields'
import { useState } from 'react'
import Table from '@/components/Table'
import { SingleSelectDropdown, MultiSelectDropdown } from '@/components/Dropdown'
import { data } from '@/services/dummyData';
import { dropdownData } from '@/components/dummyData'

export default function Home() {
  const [val,setVal]= useState([20,50]);
  const [selectedCompany,setSelectedCompany] = useState({name:null, id:null});
  const [selectedCompanyList, setSelectedCompanyList] = useState([]);

  const transactionsColumnList = [
    { name: "Date", width: "10%", 
    // frozenColumn:true,
    minWidth: '160px', key: "date", style: "flex" },
    {
      name: "Reference Id",
      width: "10%",
      key: "reference_id",
      secondRowKey:"received_payment",
      style: "flex",
      // frozenColumn:true ,
      minWidth: '120px',
    },
    {
      name: "Amount",
      width: "10%",
      key: "received_payment",
      style: "flex",
      minWidth: '160px',
    },
    { name: "Payment Mode", width: "10%", key: "transaction_mode", minWidth: '160px', style: "flex" },
    {
      name: "Brand",
      width: "15%",
      type: "array",
      key: "brand_list",
      style: "flex",
      minWidth: '160px'
    },
    { name: "PI Number", width: "20%", minWidth: '160px', key: "pi_no", style: "flex", textAlign: 'center' },
    { name: "PI Date", width: "10%", minWidth: '160px', key: "pi_date",type:"date", style: "flex", textAlign: 'center' },
    { name: "Company", width: "15%", minWidth: '140px', key: "",type:"dropdown", style: "flex", textAlign: 'center' },


  ];

  const transactionsColumnSortState = {
    reference_id: '',
    date: '',
    received_payment: '',
    transaction_mode: '',
    pi_no: '',
    pi_date: ''
  }
  return (
    <div>My Profile
        <div className='w-full'>
          <Table
          className="overflow-y-auto overflow-x-scroll mx-4 h-[30rem]"
          columns={transactionsColumnList}
          isSortingNeeded
          isMultiRowSelectNeeded
          rowIdKey='reference_id'
          rows={data}
          handleRowClick={(key, row) => {}}
          handleTableSort={(key, order) => {}}
          sortState={transactionsColumnSortState}
          /></div>

          <p>table end</p>

          {/* <SingleSelectDropdown
                  padding='0.25rem'
                  options={dropdownData}
                  optionName='company_name'
                  optionID='company_id'
                  placeholder='Select Company'
                  setselected={(name, id) => {
                    setSelectedCompany({name:name,id:id})
                  }}
                  selected={selectedCompany.name}
                  selectedOptionId={selectedCompany.id}
                  className="rounded-full w-[25rem] h-[30px] mb-[5px]"
                /> */}

          {/* <MultiSelectDropdown
              options={dropdownData}
              optionName='company_name'
              optionID='company_id'
              setselected={(entry) => {
                setSelectedCompanyList(entry)
              }}
              selected={selectedCompanyList}
              placeholder='Select Company '
              padding='0.2rem 0.5rem'
              onFilterCancel={()=>{}}
              handleDropdownSelections={()=>{}}
              className="rounded-full w-[25rem] m-0 text-[1rem] bg-transparent"
            /> */}
               </div>

  )
}
