'use client'

import Image from 'next/image'
import Button from '@/components/Button'
import Input from '@/components/InputFields'
import { useState } from 'react'

export default function Home() {
  const [val,setVal]= useState([20,50])
  return (
    <div className='row-span-full col-span-full mt-4 ml-6'>
        <h1 className='text-[18px] font-semibold mb-6 '>Settings</h1>

        <span className='grid gap-4'><Input type='text' className="w-[18rem] h-[4rem]" label='Roll Number*' placeholder='Your college roll number' />
        <Input type='number' className="w-[18rem] h-[4rem]" label='Mobile Number*' placeholder='Your mobile number' />
        <Input type='email' className="w-[18rem] h-[4rem]" label='Personal Email Id' placeholder='Your personal email id' />
        <Input type='email' className="w-[18rem] h-[4rem]" label='College Email Id*' placeholder='Your college email id' />
        </span>
               </div>

  )
}
