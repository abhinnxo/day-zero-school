import { usePathname } from 'next/navigation';
import { useMediaQuery } from 'react-responsive'
import dynamic from 'next/dynamic';
import { useState, useEffect } from 'react';
import LocalStorageService from '@/services/LocalStorageHandler'

const Navbar = dynamic(() => import('./NavbarForMobile'), { ssr: false });
const Sidebar = dynamic(() => import('./Sidebar'), { ssr: false });
const localStorage = LocalStorageService.getService()

export default function Layout({ children }) {
  const currentUrl = usePathname()
  const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
    // This callback is only executed on the client side
    return matches;
  })
  const [sidebarContent, setSidebarContent] = useState([]);
  const [userName, setUserName] = useState(null);



  const userSidebarContent = [
    { name: 'Jobs', path: '/', icon: '/business_center.svg', selectedTabIcon: '/sel-business_center.svg' },
    { name: 'My Profile', path: '/user/my-profile', icon: '/account_circle.svg', selectedTabIcon: '/sel-account_circle.svg',
        subMenu: [
          { name: 'Personal Details', path: '/personal-details' },
          { name: 'Education', path: '/education'},
          { name: 'Skills', path: '/skills'},
    ], },
    { name: 'Settings', path: '/user/settings', icon: '/settings.svg', selectedTabIcon: '/sel-settings.svg' },
    { name: 'Contact', path: '/user/contact', icon: '/contact_support.svg', selectedTabIcon: '/sel-contact_support.svg' },
  ];

  const adminSidebarContent = [
    { name: 'Dashboard', path: '/', icon: '/dashboard.svg', selectedTabIcon: '/sel-dashboard.svg' },
    { name: 'Jobs', path: '/', icon: '/business_center.svg', selectedTabIcon: '/sel-business_center.svg' },
    { name: 'Students', path: '/user/student', icon: '/student.svg', selectedTabIcon: '/sel-student.svg' },
    { name: 'Companies', path: '/user/settings', icon: '/apartment.svg', selectedTabIcon: '/sel-apartment.svg' },
    { name: 'Inbox', path: '/user/settings', icon: '/mail.svg', selectedTabIcon: '/sel-mail.svg' },
    { name: 'Settings', path: '/user/settings', icon: '/settings.svg', selectedTabIcon: '/sel-settings.svg' },
  ];


  useEffect(() => {
    // const user = localStorage.getUser()
    const user = { name: "Alisha" };
    const userRole = 'user';
    if (userRole === 'user') {
      setSidebarContent(userSidebarContent)
    } else {
      setSidebarContent(adminSidebarContent)
    }
    if (user != null || user != undefined) {
      setUserName(user.name)
    }
  }, [])

  
  return (
    // in case of login page , dont display sidebar/topbar
    <>{currentUrl == '/login' ? <main>{children}</main> : !isMobile ? 
    <Sidebar children={children} sidebarContent={sidebarContent} userName={userName}/> : 
    <Navbar children={children} sidebarContent={sidebarContent} userName={userName}/>}</>
  )
}
