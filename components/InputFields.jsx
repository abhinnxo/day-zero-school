'use client'

import { DatePicker, Slider, Switch, Progress } from 'antd';
import dayjs from 'dayjs';
import { MdSearch } from 'react-icons/md'
import { useEffect } from 'react';


const Input = ({
  type,
  label,
  value,
  onChange,
  min,
  max,
  onBlur,
  readOnly,
  placeholder,
  disabled,
  dateFormat,
  step,
  checked,
  ...restProps
}) => {
  let inputField = null;
  const sliderStyle = {
    trackStyle: { backgroundColor: '#7F56D9' }, // Set the color of the track
    handleStyle: { borderColor: '#7F56D9', backgroundColor: '#7F56D9' }, // Set the color of the handle
  };

  useEffect(() => {
    if (type == "labelInsideInput") {
      const label1 = "labelInsideInput_" + label;
      const inLabel = "InputWithLabel_" + label;
      // Get references to the label and div elements
      const labelElement = document.getElementById(label1);
      const divElement = document.getElementById(inLabel);

      // Get the computed width of the label
      const labelWidth = labelElement?.offsetWidth;
      if (labelElement && divElement) {
        // Apply the same padding to the div element
        divElement.style.paddingLeft = `${labelWidth + 10}px`;
      }
    }
  }, [])


  if (type === 'textarea') {
    inputField = (<textarea
      className={`p-2 text-sm rounded-md border-1 focus-visible:outline-softviolet`}
      id={label}
      onChange={onChange}
      onBlur={onBlur}
      placeholder={placeholder}
      min={min}
      max={max}
      disabled={disabled}
      readOnly={readOnly}
    >
      {value}
    </textarea>)
  } else if (type === 'date') {
    inputField = (
      <div className='rounded-md border-1 focus-within:border-softviolet'><DatePicker onChange={onChange} format={dateFormat} bordered={false} /></div>
    )
  } else if (type === 'progress') {
    inputField = (
      <Progress percent={value} type="circle" size={80} strokeColor='#7F56D9' />
    )
  } else if (type === 'switch') {
    const switchStyle = {
      backgroundColor: value ? '#7F56D9' : '#D0D5DD', // Set the background color when switch is on or off
      width: '50%'
    };
    inputField = (
      <span className='pl-2 w-[5rem]'><Switch checked={value} onChange={onChange}
        style={switchStyle} /></span>
    )
  } else if (type === 'range') {
    inputField = (<div className='flex flex-col'>
      <span className='text-sm text-textlightgrey'>₹ {value[0]} - ₹ {value[1]}</span>
      <Slider
        className='mt-[-1px]'
        defaultValue={value}
        onChange={onChange}
        range
        tooltip={{
          open: false,
        }}
        {...sliderStyle}
      />
    </div>);
  } else if (type === 'search') {
    inputField = (<>
      {/* <MdSearch className='text-gray3 absolute top-[6px] right-1' size={20}/>  */}
      <input
        className={` pl-2 h-full w-full text-sm rounded-md border-1  focus-visible:outline-softviolet`}
        type={type}
        id={label}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        placeholder={placeholder}
        onClick={() => restProps.onClick ? restProps.onClick() : {}}
      /></>);
  } else if (type === 'labelInsideInput') {
    inputField = (<>
      {label ? <label id={`labelInsideInput_${label}`} className='m-2 text-textgrey3 text-xs capitalize absolute ' htmlFor={label}>
        {label}
      </label> : null}
      <input
        className={` pl-2 h-full w-full text-sm border-1 rounded-md   focus-visible:outline-softviolet`}
        type={type}
        id={`InputWithLabel_${label}`}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        placeholder={placeholder}
        onClick={() => restProps.onClick ? restProps.onClick() : {}}
      /></>);
  } else {
    inputField = (<input
      className={` pl-2 h-full w-full text-sm rounded-md ${restProps.error ? 'border-red-500 shadow-md shadow-red-200 ' : ''} border-1  focus-visible:outline-softviolet`}
      type={type}
      id={label}
      value={value}
      onChange={onChange}
      onBlur={onBlur}
      placeholder={placeholder}
      disabled={disabled}
      readOnly={readOnly}
      checked={checked}
      onClick={() => restProps.onClick ? restProps.onClick() : {}}
    />);
  }


  return (
    <div
      className={`flex relative flex-col ${restProps.className ? restProps.className : ''
        }`}
    >
      {type != "labelInsideInput" && label ? <label className='m-2 text-xs capitalize ' htmlFor={label}>
        {label}
      </label> : null}
      {inputField}
    </div>
  )
}

export default Input
