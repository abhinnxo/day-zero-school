import React, { useState, useRef, useEffect } from 'react'
import { IoMdArrowDropdown, IoMdArrowDropup  } from 'react-icons/io'
//Assets
import { RiCheckboxCircleFill } from 'react-icons/ri'
import { MdOutlineClose, MdSearch } from 'react-icons/md';


/* Multiple Select Dropdown */
export function MultiSelectDropdown({
  options,
  optionName,
  optionID,
  selected,
  setselected,
  activeOptionIndex,
  required,
  color,
  padding,
  label,
  placeholder,
  disabled,
  className,
  ...props
}) {
  const [isOpen, setIsOpen] = useState(false)
  const [localOptions, setLocalOptions] = useState()
  const [selectedOption, setSelectedOption] = useState(selected)
  const [searchString, setSearchString] = useState('')
  let selectedOptionsLength = selected.length
  const clickRef = useRef()
  const toggling = () => setIsOpen(!isOpen)
  const filterOptions = (e) => {
    let filteredOptions = options.filter((option) =>
      option[optionName].toLowerCase().includes(e.target.value.toLowerCase())
    )
    if (filteredOptions.length > 0) {
      setLocalOptions(filteredOptions)
    } else {
      setLocalOptions([])
    }
  }
  const handleSelect = async (e, option) => {
    e.stopPropagation()

    const found = selectedOption.find(
      (s) => s[optionName] === option[optionName]
    )
    if (found) {
      const newSelected = selectedOption.filter(
        (s) => s[optionName] !== option[optionName]
      )
      setSelectedOption(newSelected)
      setselected(newSelected)

      return
    }
    const existSelection = [...selected, option]
    setSelectedOption(existSelection)
    setselected(existSelection)
  }

  const closeDropdown = (e) => {
    setIsOpen(false)
  }
  // Track events outside scope
  const clickOutside = (e) => {
    if (clickRef.current.contains(e.target)) {
      return
    }
    // outside click
    setIsOpen(false)
  }
  useEffect(() => {
    //reset local options
    setLocalOptions(options)
    //cleaning last searched
    setSearchString('')
    if (isOpen) {
      document.addEventListener('mousedown', clickOutside)
    }
    return () => {
      document.removeEventListener('mousedown', clickOutside)
    }
  }, [selectedOption, isOpen])
  console.log("len",selectedOptionsLength)

  return (
    <div
      className={`${selectedOptionsLength != 0?'border-softviolet':''} ${
        disabled
          ? `dropdown dropdownDisabled `
          : `dropdown `
      } ${className}`}
      ref={clickRef}
    >
      <div
        onClick={toggling}
        style={{ padding: padding, color: color }}
        className={`innerDiv multipleSelectInnerDiv `}
      >

        {isOpen ? (
          <input
            onChange={(e) => {
              filterOptions(e)
              setSearchString(e.target.value)
              /* setSelectedOption(e.target.value); */
            }}
            placeholder={placeholder}
            value={searchString}
            required={required}
            autoFocus
            disabled={disabled}
            className={`ml-2 `}
          />
        ) : (
          <div className='selectedOptionList'>
            {selected.length == 0 ? (
              <input className='w-full' placeholder={placeholder}/>
            ) : (
              <p className={`${selectedOptionsLength != 0?'text-softviolet':''}`} title={selected.map((item) => item[optionName])}>
                {selected[selectedOptionsLength - 1][optionName]}{' '}
                {selectedOptionsLength > 1 && `+ ${selected.length - 1}`}
              </p>
            )}
          </div>
        )}
        <div className='selectedOptionList'>
          {selected.length>0?<span
            onClick={(e) => {
              e.stopPropagation()
              setselected([])
              setSelectedOption([])
              props.onFilterCancel()
            }}
          >
            <MdOutlineClose title='Cancel' size={20} color="#757575"/>
            <MdSearch size={20} title="Search" color='#757575' onClick={(e)=>{props.handleDropdownSelections(); e.stopPropagation()}} />

          </span>:null}

        </div>
      </div>{' '}
      {!isOpen && (
        <ul
          onMouseLeave={() => closeDropdown()}
          style={{
            listStyle: 'none',
            padding: '0',
          }}
          className='ul top-[2.2rem] left-0 rounded-md overflow-hidden'
        >
          {localOptions !== undefined && localOptions.length > 0 ? (
            localOptions.map((option, index) => {
              const isSelected = selectedOption.find(
                (s) => s[optionName] === option[optionName]
              )
              return (
                <li onClick={(e) => handleSelect(e, option)} key={index}>
                  <div className='multiselectOption'>
                    {isSelected ? (
                      <p className='selectedText'>
                        <RiCheckboxCircleFill color='var(--blue)' />
                      </p>
                    ) : (
                      <p className='selectedText'>
                        <RiCheckboxCircleFill />
                      </p>
                    )}
                    <p className={`m-0`}
                    style={{color:option.color}}>{option[optionName]}</p>
                  </div>
                </li>
              )
            })
          ) : (
            <li style={{ cursor: 'no-drop' }}>
              <div className='multiselectOption'>
                <p className='m-0 dark:text-white'>No options available</p>
              </div>
            </li>
          )}
        </ul>
      )}
    </div>
  )
}

// MultiSelectDropdown.defaultProps = {
//   dropdownHeader: 'Company XZY',
//   options: ['Company JQL2', 'Company AB', 'Company 1'],
//   label: 'Select Field',
// }

export function SingleSelectDropdown({
  options,
  optionName,
  optionID,
  selected,
  setselected,
  activeOptionIndex,
  required,
  padding,
  label,
  dropdownLabel,
  placeholder,
  disabled,
  className,
  ...props
}) {
  const [isOpen, setIsOpen] = useState(false)
  const [localOptions, setLocalOptions] = useState()
  const [selectedOption, setSelectedOption] = useState('')
  const inputRef = useRef();
  const singleSelectClickRef = useRef();

  const toggling = () => {
    if (!disabled) {
      if (isOpen) {
        inputRef.current.blur()
      } else {
        inputRef.current.focus()
      }

      setIsOpen(!isOpen)
    }
  }

  // Track events outside scope
  const clickOutside = (e) => {
    if (singleSelectClickRef.current.contains(e.target)) {
      return
    }
    // outside click
    setIsOpen(false)
  }

  const filterOptions = (e) => {
    let filteredOptions = options.filter((option) =>
      option[optionName].toLowerCase().includes(e.target.value.toLowerCase())
    )
    if (filteredOptions.length > 0) {
      setLocalOptions(filteredOptions)
    } else {
      setLocalOptions([])
      setSelectedOption(null)
    }
  }
  const onOptionClicked = (value, i) => {
    setSelectedOption(value[optionName])
    // activeOptionIndex(value[optionID])
    setselected(value[optionName], value[optionID])
    setIsOpen(false)
  }
  useEffect(() => {
    setLocalOptions(options)

    if (isOpen) {
      document.addEventListener('mousedown', clickOutside)
    }
    return () => {
      document.removeEventListener('mousedown', clickOutside)
    }
    setSelectedOption(selected)
  }, [isOpen, selected, options])

  console.log("drop",selectedOption)
  return (
    <div className={`flex flex-col `} ref={singleSelectClickRef}>
      {dropdownLabel ? (
        <label className='m-2 text-xs capitalize' htmlFor={dropdownLabel}>
          {dropdownLabel}
        </label>
      ) : null}
      <div
        className={`border-1 dropdown pl-[5px] ${selectedOption != ''?'border-softviolet':''} ${
          disabled ? 'dropdownDisabled' : ''
        } ${className}`}
        unselectable='true'
      >
        <div
          onClick={toggling}
          style={{ padding: padding}}
          className={`innerDiv `}
        >
          <input
            onChange={(e) => {
              filterOptions(e)
              setSelectedOption(e.target.value)
            }}
            placeholder={placeholder}
            value={selected}
            required={required}
            disabled={disabled}
            className={`ml-2 ${selectedOption != ''?'text-softviolet':''}`}
            ref={(input) => (inputRef.current = input)}
          />
          {isOpen ? (
            <IoMdArrowDropup color="#757575" />
          ) : (
            <IoMdArrowDropdown color="#757575" />
          )}
        </div>{' '}
        {isOpen && (
          <ul className={`ul p-2 w-full top-[2.2rem] left-0 rounded-md overflow-hidden`}>
            {localOptions !== undefined && localOptions.length > 0 ? (
              localOptions.map((option, i) => {
                let selectedStyle='';
                if(option[optionID] === props.selectedOptionId){
                  selectedStyle ='bg-lightviolet '
                }
                return (
                <li
                  className={`p-2 ${selectedStyle}`}
                  style={{color:option.color}}
                  onClick={() => onOptionClicked(option, i)}
                  key={i}
                >
                  {option[optionName]}
                </li>
              )})
            ) : (
              <li>No Options</li>
            )}
          </ul>
        )}
      </div>
    </div>
  )
}

// SingleSelectDropdown.defaultProps = {
//   options: ['Option 1', 'Option 1', 'Option 1', 'Long Item Labelsdjbfbh'],
//   label: 'Select Field',
//   placeholder: 'Select',
//   padding: '0.35rem 1rem',
//   fontsize: '0.875rem',
//   activeOptionIndex: () => {},
//   setselected: () => {},
//   disabled: false,
// }
