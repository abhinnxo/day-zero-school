const Button = ({
    children,
    className,
    size,
    variant,
    customText,
    onClick,
    disabled,
  }) => {
    let style
    let variantStyle
    switch (size) {
      case 'extrasmall':
        style = 'w-extrasmall h-[30px]'
        break
      case 'small':
        style = 'w-small h-[35px]'
        break
      case 'medium':
        style = 'w-medium h-[40px]'
        break
      case 'semimedium':
        style = 'w-semimedium h-[40px]'
        break
      case 'large':
        style = 'w-large h-[40px]'
        break
      default:
        style = 'fit-content h-[40px] px-[12px] py-[2px]'
    }
  
    switch (variant) {
      case 'transparent':
        variantStyle = `${disabled ? 'bg-lightgrey text-white cursor-not-allowed' : 'white border-1 border-lightgrey text-textgrey2'}`
        break
      case 'inverted':
        variantStyle = `${disabled ? 'bg-lightgrey text-white cursor-not-allowed' : 'bg-[#f379201c] text-white'}`
        break
      default:
        variantStyle = `${disabled ? 'bg-lightgrey text-white cursor-not-allowed' : 'bg-softviolet text-white'}`
    }
  
    return (
      <button
        className={`${className ? className : ''} ${style} ${variantStyle} rounded-md text-[12px]`}
        onClick={onClick}
        disabled={disabled}
      >
        {children}
      </button>
    )
  }
  
  export default Button
  