import { FaFileInvoice, FaRegUserCircle } from "react-icons/fa";
import { MdOutlineSettings ,MdOutlineEdit , MdOutlineNotificationsNone } from "react-icons/md";
import { MdLogout } from 'react-icons/md'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter, usePathname } from 'next/navigation';
import { useEffect, useState } from 'react'
import { Badge } from 'antd';
import Input from "./InputFields";



const Sidebar = ({children, sidebarContent, userName}) =>{
    const [showSubContent, setShowSubContent] = useState(false);
    const router = useRouter()
    const currentUrl = usePathname()
      
      useEffect(()=>{
        if(currentUrl === '/user/my-profile' || currentUrl === '/education'  || currentUrl ==='/skills' || currentUrl === '/personal-details'){
          setShowSubContent(true)
        }else{
          setShowSubContent(false)
        }
      },[currentUrl])
    
      const logout = () => {
        localStorage.clearToken()
        router.push('/login')
      }

    return(
        <div className='flex flex-col h-full relative'>
        {/* top bar starts here */}
      <div className='w-full flex items-center border-b-1 h-[4rem] justify-end pr-6'>

        <Badge count={7} ><MdOutlineNotificationsNone className="text-gray3 cursor-pointer" size={25}/></Badge>
        <span className="w-[2.5rem] h-[2.5rem] border-1 rounded-full ml-[2rem]"></span>

      </div>

      <div className='flex'>

        {/* sidebar starts here..only display it in web view */}
      <div className='flex flex-col gap-8 py-8 text-textgrey border-r-1 basis-[15%] min-w-[13rem]'>
      
        {/* Menu */}
        <nav>
          <ul className='flex flex-col font-light'>
            {sidebarContent.map((menuItem) => {
              return (
                <li key={menuItem.name}>
                  <div className='px-3 py-1 pl-0'>
                    <Link
                      className={`relative gap-1 rounded-md py-2 pr-2 items-center flex
                      ${currentUrl == menuItem.path?'font-medium bg-lightviolet text-violet':''} ml-2 w-[98%] pl-4 cursor-pointer`}
                      href={
                        menuItem.path
                      }
                      onClick={()=>{menuItem.name ==='My Profile'?setShowSubContent(!showSubContent):{}}}
                    >
                     <span className='text-l mr-2'>
                      <Image alt={menuItem.name} src={currentUrl == menuItem.path?menuItem.selectedTabIcon:menuItem.icon} width={25} height={25}
                      /></span>
                      <span
                      >
                        {menuItem.name}
                      </span>
                    </Link>
                  </div>
                </li>
              )
            })}
          </ul>
        </nav>
        {/* Logout */}
        <div className='flex items-center p-2 mx-auto mt-auto text-center rounded  w-[80%]'>
          <span className='text-[14px] max-w-[120px] mr-4'>Logout</span>

          <MdLogout
            className='cursor-pointer text-ornateorange'
            title='Logout'
            size={20}
            onClick={() => logout()}
          />
        </div>
      </div> 

      {showSubContent?<div className='flex flex-col py-8 text-textgrey3 border-r-1 basis-[15%] min-w-[13rem]'>
        <div className='rounded-full border-1 h-[8rem] w-[8rem] mr-auto ml-auto relative'>
          <div className='z-1 flex justify-center items-center h-[2rem] w-[2rem] right-0 bg-white rounded-full border-1 absolute'>
            <MdOutlineEdit className='cursor-pointer' title='Edit Photo'/>
          </div>
        </div>
        <span className=' my-2 flex flex-col justify-center items-center'>
        <span className=' text-textgrey font-semibold'>{userName}</span>
        <span className='text-[14px]'>Roll No:</span></span>
        {/* Menu */}
        <nav>
          <ul className='flex flex-col font-light'>
            {[
              { name: 'Personal Details', path: '/personal-details' },
              { name: 'Education', path: '/education'},
              { name: 'Skills', path: '/skills'},
            ].map((menuItem, listIndex) => {
              return (
                <li key={menuItem.name}>
                  <div className='px-3 py-1 pl-0'>
                    <Link
                      className={`relative gap-1 text-[16px] active:text-violet font-normal active:font-semibold sidebar_subcontents rounded-md py-2 pr-2 items-center flex ml-2 w-[98%] pl-4 cursor-pointer`}
                      href={
                        menuItem.path
                      }
                    >
                      <span
                      >
                        {menuItem.name}
                      </span>
                    </Link>
                  </div>
                </li>
              )
            })}
          </ul>
        </nav>
      </div> :null}

      {/* children pages */}
      <section className=' w-full h-[calc(100vh-4rem)] overflow-scroll'>
        {children}</section></div>
    </div>
    )
}

export default Sidebar;
