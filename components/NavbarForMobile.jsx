import { BiMenu } from 'react-icons/bi';
import { useState } from 'react';
import { MdLogout, MdKeyboardArrowUp, MdKeyboardArrowDown } from 'react-icons/md'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter, usePathname } from 'next/navigation';

const Navbar = ({ children, sidebarContent, userName }) => {
    const [isOpen, setIsOpen] = useState(false);
    const [showSubContent, setShowSubContent] = useState(false);
    const toggleMenu = () => {
        setIsOpen(!isOpen);
    };
    const currentUrl = usePathname()



    return (
        <div className="wrapper">
            <div className={`${isOpen ? 'section' : ''}`}>
                <div className="flex items-center h-[3.5rem] p-2 border-b-1">
                    <BiMenu size={30} className='hamburger cursor-pointer' onClick={toggleMenu} />
                </div>

                <div>

                    {/* Mobile menu */}
                    {isOpen ? <div className={`sidebar bg-[#F4F0FD] z-30 flex flex-col`}>
                        <span className='border-1 h-[6rem] w-[6rem] mb-2 mx-auto rounded-full'></span>
                        <p className='mx-auto'>{userName}</p>

                        <nav>
                            <ul className='flex flex-col font-light'>
                                {sidebarContent.map((menuItem) => {
                                    return (
                                        <li key={menuItem.name}>
                                            <div className='px-3 py-1 pl-0'>
                                                <Link
                                                    className={`relative gap-1 rounded-md py-2 pr-2 items-center flex
                      ${currentUrl == menuItem.path ? 'font-medium bg-lightviolet text-violet' : ''} ml-2 w-[98%] pl-4 cursor-pointer`}
                                                    href={
                                                        menuItem.path
                                                    }
                                                    onClick={() => { menuItem.name === 'My Profile' ? setShowSubContent(!showSubContent) : {} }}
                                                >
                                                    <span className='text-l mr-2'>
                                                        <Image alt={menuItem.name} src={currentUrl == menuItem.path ? menuItem.selectedTabIcon : menuItem.icon} width={25} height={25}
                                                        /></span>
                                                    <span className='flex justify-between w-full'
                                                    >
                                                        {menuItem.name}
                                                        {menuItem.name === 'My Profile' ? showSubContent ? <MdKeyboardArrowUp className='mt-[2px]' size={20} /> : <MdKeyboardArrowDown className='mt-[2px]' size={20} /> : null}

                                                        
                                                    </span>
                                                </Link>
                                                <div className='flex'>
                                                    {/* {showSubContent && menuItem.subMenu?<div className='w-[1px] border-1 ml-4 border-violet'></div>:null} */}
                                                            <ul className='flex flex-col font-light ml-9'>
                                                                {showSubContent && menuItem.subMenu ? menuItem.subMenu.map((menuItem, listIndex) => {
                                                                    return (
                                                                        <li key={menuItem.name}>
                                                                            <div className='px-3'>
                                                                                <Link
                                                                                    className={`relative active:text-violet font-normal active:font-semibold sidebar_subcontents rounded-md py-2 pr-2 items-center flex ml-2 w-[98%] pl-4 cursor-pointer`}
                                                                                    href={
                                                                                        menuItem.path
                                                                                    }
                                                                                >
                                                                                    <span
                                                                                    >
                                                                                        {menuItem.name}
                                                                                    </span>
                                                                                </Link>
                                                                            </div>
                                                                        </li>
                                                                    )
                                                                }) : null}
                                                            </ul>
                                                        </div>
                                            </div>
                                        </li>
                                    )
                                })}
                            </ul>
                        </nav>

                        {/* Logout */}
                        <div className='flex items-center p-2 mx-auto mt-auto text-center rounded  w-[80%]'>
                            <span className='text-[14px] max-w-[120px] mr-4'>Logout</span>

                            <MdLogout
                                className='cursor-pointer text-ornateorange'
                                title='Logout'
                                size={20}
                                onClick={() => logout()}
                            />
                        </div>
                    </div> : null}
                </div>
                {/* children pages */}
                <section className={`${isOpen ? 'left-[15rem] blur-sm' : ''} h-[calc(100vh-4rem)] p-2 overflow-scroll`}>
                    {children}</section>
            </div>

        </div>
    )
}

export default Navbar;