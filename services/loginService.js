import Router from "next/router";
import { loginInstance } from "./ApiHandler";
import LocalStorageService from "./LocalStorageHandler";


// import {URL, MAIN } from "./constants";

const localStorage = LocalStorageService.getService();

export const onLogin=(username,password) =>{
            return loginInstance.post('api-token-auth/',{
                username:username,
                password:password
            }).then(res=>{
                console.log(res.data)
                
                if(res.data.status.code == 200){
                    console.log(res.data.data.output)
                    localStorage.setUser(res.data.data.output)
                    localStorage.setAccessToken(res.data.data.output.token)
                    Router.push('/');
                }else{
                    alert(res.data.status.description)
                }
            }).catch();
        }

