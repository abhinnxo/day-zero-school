import { axiosInstance } from '../ApiHandler'
import LocalStorageService from '../LocalStorageHandler'

const localStorage = LocalStorageService.getService()

//Constants
const FORM_API_CONFIG = 'user_application_form/'
