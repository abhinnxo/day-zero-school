// LocalStorageService.js

const LocalStorageService = (function () {
  var _service
  function _getService() {
    if (!_service) {
      _service = this
      return _service
    }
    return _service
  }

  function _setAccessToken(accessToken) {
    localStorage.setItem('access_token', accessToken)
  }

  function _setRefreshToken(refreshToken) {
    localStorage.setItem('refresh_token', refreshToken)
  }

  function _getAccessToken() {
    return localStorage.getItem('access_token')
  }

  function _getRefreshToken() {
    return localStorage.getItem('refresh_token')
  }

  function _setUser(user) {
    localStorage.setItem('user', JSON.stringify(user))
  }

  function _getUser() {
    return JSON.parse(localStorage.getItem('user'))
  }

  function _setUserRole(user) {
    localStorage.setItem('user_role', JSON.stringify(user))
  }

  function _getUserRole() {
    return JSON.parse(localStorage.getItem('user_role'))
  }

  

  function _clearToken() {
    localStorage.removeItem('access_token')
    localStorage.removeItem('refresh_token')
  }

  return {
    getService: _getService,
    setAccessToken: _setAccessToken,
    getAccessToken: _getAccessToken,
    setRefreshToken: _setRefreshToken,
    getRefreshToken: _getRefreshToken,
    setUser: _setUser,
    getUser: _getUser,
    setUserRole: _setUserRole,
    getUserRole: _getUserRole,
    clearToken: _clearToken,
  }
})()

export default LocalStorageService
