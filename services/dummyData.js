export const data = [
    {
        "reference_id": "000206065470",
        "received_payment": "20.06 L",
        "date": "2023-08-01",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2024-9370",
        "pi_date": "2023-07-31",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000372804711",
        "received_payment": "2.55 L",
        "date": "2023-07-27",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2024-9316",
        "pi_date": "2023-07-27",
        "brand_list": [
            "Enphase","RenewSys","Canadian"
        ]
    },
    {
        "reference_id": "000100813113",
        "received_payment": "18.95 L",
        "date": "2023-07-20",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2024-9259",
        "pi_date": "2023-07-20",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000105330492",
        "received_payment": "2.93 L",
        "date": "2023-07-05",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2024-9088",
        "pi_date": "2023-07-04",
        "brand_list": [
            "Fronius"
        ]
    },
    {
        "reference_id": "000382371037",
        "received_payment": "20.74 L",
        "date": "2023-07-04",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2024-9076",
        "pi_date": "2023-07-04",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000182364915",
        "received_payment": "83.92 K",
        "date": "2023-06-28",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2024-9029",
        "pi_date": "2023-06-28",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000200450183",
        "received_payment": "23.8 L",
        "date": "2023-06-22",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2024-8954",
        "pi_date": "2023-06-22",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000363311618",
        "received_payment": "45.54 L",
        "date": "2023-05-29",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2024-8712",
        "pi_date": "2023-05-29",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000363311618",
        "received_payment": "45.54 L",
        "date": "2023-05-29",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2024-8708",
        "pi_date": "2023-05-29",
        "brand_list": [
            "RenewSys"
        ]
    },
    {
        "reference_id": "000294263302",
        "received_payment": "17.82 L",
        "date": "2023-05-09",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2024-8496",
        "pi_date": "2023-05-09",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000290867989",
        "received_payment": "61.42 K",
        "date": "2023-04-20",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2024-8312",
        "pi_date": "2023-04-20",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000130048263",
        "received_payment": "23.68 L",
        "date": "2023-04-10",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2024-8196",
        "pi_date": "2023-05-02",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000200805680",
        "received_payment": "19.35 L",
        "date": "2023-03-22",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-8020",
        "pi_date": "2023-03-21",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000183673302",
        "received_payment": "17.42 L",
        "date": "2023-03-09",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-7855",
        "pi_date": "2023-03-09",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000278159611",
        "received_payment": "26.4 L",
        "date": "2023-03-01",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-7754",
        "pi_date": "2023-03-06",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000185647281",
        "received_payment": "26.8 K",
        "date": "2023-02-08",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-7600",
        "pi_date": "2023-02-08",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000128178759",
        "received_payment": "17.68 L",
        "date": "2023-02-06",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-7573",
        "pi_date": "2023-02-04",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000000000074",
        "received_payment": "50.52 L",
        "date": "2023-02-04",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-7574",
        "pi_date": "2023-02-04",
        "brand_list": [
            "RenewSys"
        ]
    },
    {
        "reference_id": "000274996544",
        "received_payment": "18.18 K",
        "date": "2023-01-21",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-7502",
        "pi_date": "2023-01-21",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000222113501",
        "received_payment": "67.19 K",
        "date": "2023-01-05",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-7398",
        "pi_date": "2023-01-04",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000295616708",
        "received_payment": "17.66 L",
        "date": "2022-12-28",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-7343",
        "pi_date": "2022-12-28",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000000000066",
        "received_payment": "6.21 L",
        "date": "2022-12-17",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-7251",
        "pi_date": "2022-12-16",
        "brand_list": [
            "RenewSys"
        ]
    },
    {
        "reference_id": "000324261704",
        "received_payment": "45.0 L",
        "date": "2022-12-16",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-7251",
        "pi_date": "2022-12-16",
        "brand_list": [
            "RenewSys"
        ]
    },
    {
        "reference_id": "000394729000",
        "received_payment": "18.11 L",
        "date": "2022-11-11",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-6930",
        "pi_date": "2022-11-08",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000113967189",
        "received_payment": "1.03 L",
        "date": "2022-10-21",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-6822",
        "pi_date": "2022-10-19",
        "brand_list": [
            "Fronius"
        ]
    },
    {
        "reference_id": "AXISP00327382626",
        "received_payment": "61.01 K",
        "date": "2022-10-11",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-6628",
        "pi_date": "2022-10-17",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000170555752",
        "received_payment": "19.09 L",
        "date": "2022-10-15",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-6628",
        "pi_date": "2022-10-17",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000294013143",
        "received_payment": "45.86 L",
        "date": "2022-09-30",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-6677",
        "pi_date": "2022-09-30",
        "brand_list": [
            "RenewSys"
        ]
    },
    {
        "reference_id": "000187707786",
        "received_payment": "5.0 L",
        "date": "2022-09-29",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-6677",
        "pi_date": "2022-09-30",
        "brand_list": [
            "RenewSys"
        ]
    },
    {
        "reference_id": "000198106005",
        "received_payment": "18.62 K",
        "date": "2022-09-26",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-6685",
        "pi_date": "2022-09-22",
        "brand_list": [
            "Enphase"
        ]
    },
    {
        "reference_id": "000349240527",
        "received_payment": "13.03 L",
        "date": "2022-08-26",
        "transaction_mode": "neft",
        "pi_no": "OAPL-2023-6377",
        "pi_date": "2022-08-10",
        "brand_list": [
            "Enphase"
        ]
    }
]